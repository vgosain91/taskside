import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

import { Shared } from '../shared';

@Injectable()

export class UserService {

  constructor (private http: Http , private Shared: Shared){ }

  registerUser(accessToken): Observable<Object>{
    let body = JSON.stringify({ access_token: accessToken });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let url = this.Shared.API_BASEURL + "/user";

    return this.http.post(url,body,headers)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

    private extractData(res: Response) {
     let body = res.json();
     return body || { };
   }

   private handleError (error: any) {
  return Observable.throw(error);
   }
}

class User {
  name: string;
  facebookId: number;
  email: string;
}
