'use strict';

import { Injectable, Inject, ReflectiveInjector } from '@angular/core';
import { Http,URLSearchParams, Response, Headers, RequestOptions,XHRConnection, XHRBackend, ConnectionBackend, BrowserXhr, ResponseOptions,  XSRFStrategy, CookieXSRFStrategy, BaseResponseOptions, BaseRequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {} from 'rxjs/add/operator/map';
import 'rxjs/Rx';

//for shared variables
import { Shared } from '../shared';

class MyCookieXSRFStrategy extends CookieXSRFStrategy {}

@Injectable()
export class TaskService {
private Shared;

  constructor(public http: Http) {
    this.Shared= new Shared();
  }

  //Delete single task from server
  deleteTaskById(taskId:string):Observable<Object> {
    //request 'DELETE /Task/:TaskId/:fbId'
    let self= this;
    let fbId = JSON.parse(localStorage.getItem('userInfo'))['facebookId'];
    let accesstoken= localStorage.getItem('permanent_Token');
    let body = JSON.stringify({ access_token: accesstoken});
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers,body:body });
    let url = self.Shared.API_BASEURL + "/task/"+ taskId + "/"+ fbId;
    return self.http.delete(url,options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  //Get unsynched tasks from server
  getUnsyncedTaskFromServer():Observable<Object>{
    //request : 'GET /Task/:time/:fbId'
    let self= this;
    let fbId = JSON.parse(localStorage.getItem('userInfo'))['facebookId'];
    let timeStamp = localStorage.getItem('lastSync') === null ? '0000000000000' : localStorage.getItem('lastSync');

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers});
    let url = self.Shared.API_BASEURL + "/task/" + timeStamp + "/"+ fbId;

    return self.http.get(url,options)
                          .map(this.extractData)
                          .catch(this.handleError);
  }


  taskUpload(note:any):Observable<Object> {
    let self= this;
    let accesstoken= localStorage.getItem('permanent_Token');
    var task = { title: note.title, value: note.value };
    let body = JSON.stringify({ access_token: accesstoken, task: note });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let url = self.Shared.API_BASEURL + "/task";
    return self.http.post(url,body,headers)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  getDeletedTasks(last_deleted_sync):Observable<Object> {
    let self= this;
    let fbId = JSON.parse(localStorage.getItem('userInfo'))['facebookId'];
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers});
    let url = self.Shared.API_BASEURL + "/deletedtasks/" + last_deleted_sync + "/"+ fbId;

    return self.http.get(url,options)
                          .map(this.extractData)
                          .catch(this.handleError);
  }


  private extractData(res: Response) {
    let body = res.json();
    console.log(res);
    //Set lastSync only after successful getUnsyncedTaskFromServer --TODO
    //set lastsync only after successful insertion in indexeddb ,otherwise it is of no use --TODO
    return body || { };
  }

  private handleError (error: any) {
    console.log(error);
    return Observable.throw(new Error(error.status));
  }
}
