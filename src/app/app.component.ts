import { Component , OnInit } from '@angular/core';
import {FacebookService ,InitParams} from 'ng2-facebook-sdk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent  {

constructor(private fb: FacebookService) {
let fbParams: InitParams = {
                                appId: '822767304493495',
                                xfbml: true,
                                version: 'v2.8'
                                };
 this.fb.init(fbParams);
  }
 }
